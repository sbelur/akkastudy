package com.cleo.apiAutomation.Trigger;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import com.cleo.lexicom.external.ILexiCom;
import com.cleo.lexicom.external.ISchedule;
import com.cleo.lexicom.external.ISchedule.TRIGGER_TYPE;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.cleo.lexicom.external.LexiComFactory;

import junit.framework.Assert;

/**
 * 
 * @author manjunathm
 *
 */

public class TriggerBase {
	ISchedule schedule = null;
	ILexiCom versaLex = null;
	String[] partner = {"Generic SSH FTP", "SSH FTP"};
	String HarmonyHome = "D:\\cleo\\5.3\\Harmony";


	/*public static void main(String args[]) throws Exception {
		TriggerBase test = new TriggerBase();
		//test.createHost("Generic SSH FTP", "SSH FTP", false);
	//	test.createTriggerItemAndVerify("FTP\\ftp_plain\\send", TRIGGER_TYPE.NewFileCreated);
		test.createTriggerItemAndVerify("Generic HSP\\myMailbox\\triggerAction", TRIGGER_TYPE.NewFileCreationFailed);
		//Thread.sleep(4000);
	//	test.removeItemTrigger("SSHFTP\\sshftp_plain\\send",true);
		//test.sshFileUpload();
		//ftpFileUpload();
		//checkIfFileExists("C:\\Users\\manjunathm\\Desktop\\mallorca\\test1.txt");
		test.close();
	}*/

	public TriggerBase() throws Exception {
		System.out.println("Getting versalex");
		versaLex = LexiComFactory.getVersaLex(LexiComFactory.HARMONY,  HarmonyHome,LexiComFactory.CLIENT_ONLY);
		System.out.println("Got versalex" + versaLex);
		schedule = versaLex.getSchedule();
	}

	public void close() throws Exception {
		System.exit(-1);
	}

	public void createHost(String preConfHost,String aliasForPreConfHost,boolean value) throws Exception{	

		String host = versaLex.activateHost(preConfHost,aliasForPreConfHost,value);
		String mailbox = "myMailbox";
		String[] path  = {host, mailbox};
		versaLex.setProperty(ILexiCom.HOST, path, "Address", "localhost");
		versaLex.setProperty(ILexiCom.HOST, path, "Port", "5022");
		versaLex.setProperty(ILexiCom.MAILBOX, path, "Username", "sshftp_plain");
		versaLex.setProperty(ILexiCom.MAILBOX, path, "Password", "cleo"); 
		versaLex.save(host);
	}
	

	public void createTriggerItemAndVerify(String hostMailBoxAction, ISchedule.TRIGGER_TYPE triggerType) throws Exception {

		ISchedule.Item  item = schedule.newItem(new String[]{hostMailBoxAction});
		item.setItemTrigger(triggerType);
		schedule.updateItem(item, true);
		String actualTriggerTYPE = item.getItemTrigger().toString();
		Assert.assertEquals("NewFileCreated", actualTriggerTYPE);

	}

	public void removeItemTrigger(String hostMailBoxAction,boolean value) throws Exception {
		schedule.removeItem(new String[]{hostMailBoxAction}, value);
	}


	/*public void runAction() throws Exception{
	/*	String host = versaLex.activateHost("Generic SSH FTP", "SSH FTP", false); // "SSH FTP" is the preferred alias;
		String mailbox = "sshftp_plain";
		String[] path  = {host, mailbox};

		   String[] commands = {"PUT \"" + versaLex.getAbsolute("outbox" + File.separator + "SSHFTP" + File.separator + "test.edi") + "\""};
		    versaLex.setProperty(ILexiCom.ACTION, path, "Commands", commands);
		    versaLex.startRun(ILexiCom.ACTION, path, null, false);
	}*/

	public static boolean checkIfFileExists(String filePathString) throws FileNotFoundException{
		File f = new File(filePathString);
		if(f.exists() && !f.isDirectory()) { 
			System.out.println("File Exists");
			return true;		 
		}else {
			throw new FileNotFoundException("File not found");
		}
	}	
}

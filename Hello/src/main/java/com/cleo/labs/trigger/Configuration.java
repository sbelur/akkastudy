package com.cleo.labs.trigger;

import org.yaml.snakeyaml.TypeDescription;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;
import org.yaml.snakeyaml.constructor.SafeConstructor;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class Configuration {

    public static enum Event {
        login, logout, file;  // these are lower case so the YAML file can be lower case
    }

    public static class Rule {
        public Event event;
        public String action;
        public String host;
        public String mailbox;

        public Rule() {
            this.event = null;
            this.action = null;
            this.host = null;
            this.mailbox = null;
        }

        public void setEvent(Event event) {
            this.event = event;
        }

        public void setAction(String action) {
            this.action = action;
        }

        public void setHost(String host) {
            this.host = host;
        }

        public void setMailbox(String mailbox) {
            this.mailbox = mailbox;
        }

        public Event getEvent() {
            return event;
        }

        public String getAction() {
            return action;
        }

        public String getHost() {
            return host;
        }

        public String getMailbox() {
            return mailbox;
        }

        public Rule(Object o) {
            try {
                @SuppressWarnings("unchecked")
                Map<String, Object> map = (Map<String, Object>) o;
                this.event = Event.valueOf(((String) map.get("event")));
                this.action = (String) map.get("action");
                if (map.containsKey("host")) {
                    this.host = (String) map.get("host");
                }
                if (map.containsKey("mailbox")) {
                    this.mailbox = (String) map.get("mailbox");
                }
            } catch (Exception e) {
                throw new IllegalArgumentException("invalid Rule");
            }
        }

        public String toString() {
            StringBuilder s = new StringBuilder();
            s.append("on ").append(event).append(" do ").append(action);
            if (host != null) {
                s.append(" when host=").append(host);
            }
            if (host != null) {
                s.append(" when mailbox=").append(mailbox);
            }
            return s.toString();
        }
    }

    public static final int DEFAULT_THREADS = 10;

    private List<Rule> rules;
    private boolean debug;
    private boolean remove;
    private int threads;
    private String persistenceMechanism;
    private boolean clone;


    private enum Persistor {
        AKKA;
        //TODO - add here any other means

        @Override
        public String toString() {
            return super.toString().toLowerCase();
        }

        private static String defaulMechanism(){
           return AKKA.toString();
        }
    }

    private static final String DEFAULT_PERSISTOR = Persistor.defaulMechanism();


    public Configuration() {
        rules = new ArrayList<>();
        debug = false;
        threads = DEFAULT_THREADS;
        persistenceMechanism = "akka";
        clone = false;
        remove = false; // As of now not cloning so remove is false - TEMPORARY - need to check why cloning was issuing errors.
    }

    public void setRules(List<Rule> rules) {
        this.rules = rules;
    }

    public void setDebug(boolean debug) {
        this.debug = debug;
    }

    public void setPersistenceMechanism(String persistenceMechanism) {
        this.persistenceMechanism = persistenceMechanism;
    }

    public void setRemove(boolean remove) {
        this.remove = remove;
    }



    public boolean getClone() {
        return clone;
    }



    public void setClone(boolean clone) {
        this.clone = clone;
    }


    public void setThreads(int threads) {
        this.threads = threads;
    }

    public List<Rule> getRules() {
        return rules;
    }

    public boolean getDebug() {
        return debug;
    }

    public boolean getRemove() {
        return remove;
    }

    public int getThreads() {
        return threads;
    }

    public String getPersistenceMechanism() {
        try {
            return Persistor.valueOf(persistenceMechanism).toString();
        }
        catch (Exception e){
            return DEFAULT_PERSISTOR;
        }
    }

    private static volatile Configuration active = new Configuration();
    private static final String RULECONF = "conf/logaction.yaml";
    private static long confModified = 0L;
    private static final long CONF_CHECK = 10000L; // 10 seconds
    private static long confChecked = 0L;

    private static void check() {
        long now = new Date().getTime();
        if (now - confChecked > CONF_CHECK) {
            confChecked = now;
            load();
        }
    }

    /**
     * Prints a debug {@code message} on {@code stdout}, which is routed
     * to {@code Harmonyd.out} or {@code VLTraderd.out} (or the upstart
     * log), if DEBUG is enabled.  To enable DEBUG, set {@code debug} to
     * {@code true} in the YAML configuration file.
     *
     * @param message the message
     */
    private static void debug(String message) {
        if (active.debug) {
            System.out.println(message);
        }
    }

    private static void load() {
        File ruleconf = new File(RULECONF);
        if (ruleconf.exists() && ruleconf.lastModified() > confModified) {
            Configuration newconf = null;
            try (FileInputStream conf = new FileInputStream(ruleconf)) {
                Constructor constructor = new Constructor(Configuration.class);
                TypeDescription description = new TypeDescription(Configuration.class);
                description.putListPropertyType("rules", Configuration.Rule.class);
                constructor.addTypeDescription(description);
//                Yaml yaml = new Yaml();
//                newconf = yaml.loadAs(conf, Configuration.class);
            } catch (Exception e) {
                e.printStackTrace();
                try (FileInputStream conf = new FileInputStream(ruleconf)) {
                    Yaml yaml = new Yaml();
                    Object parsed = yaml.load(conf);
                    @SuppressWarnings("unchecked")
                    List<Object> list = (List<Object>) parsed;
                    List<Rule> rules = new ArrayList<>();
                    for (Object o : list) {
                        rules.add(new Rule(o));
                    }
                    newconf = new Configuration();
                    newconf.setRules(rules);
                } catch (Exception ignore) {
                    ignore.printStackTrace();
                }
            }
            if (newconf != null) {
                Configuration.active = newconf;
                debug("new trigger configuration loaded");
            } else {
                debug("new trigger configuration found, but errors were detected");
            }
            confModified = ruleconf.lastModified();
        }
    }

    public static List<Rule> get(Event event) {
        return get(event, null, null);
    }

    public static List<Rule> get(Event event, String host, String mailbox) {
        check();
        List<Rule> rules = active.getRules();
        List<Rule> match = new ArrayList<Rule>(rules.size());
        for (Rule rule : rules) {
            if (rule.event == event &&
                    (rule.host == null || host != null && rule.host.equalsIgnoreCase(host)) &&
                    (rule.mailbox == null || mailbox != null && rule.mailbox.equalsIgnoreCase(mailbox))) {
                match.add(rule);
            }
        }
        return match;
    }

    public static Configuration getConfiguration() {
        return active;
    }

}

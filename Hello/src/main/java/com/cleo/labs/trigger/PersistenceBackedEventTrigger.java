package com.cleo.labs.trigger;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.cluster.Cluster;
import com.cleo.labs.trigger.cluster.support.ClusterMode;
import com.cleo.labs.trigger.cluster.support.ClusterSystem;
import com.cleo.labs.trigger.persist.akka.messages.ActionCommand;
import com.cleo.labs.trigger.persist.akka.persistor.LexEventPersistor;

/**
 * Created by sbelur on 20/09/15.
 */
public class PersistenceBackedEventTrigger extends AbstractEventTrigger {

    private static ActorSystem actorSystem;
    private static ActorRef eventPersistentActor;

    //TODO - move this to post vm startup
    static {
        // Eager so as to trigger persisted messages on JVM startup itself.
        System.out.println("Creating actor system");
        actorSystem = ActorSystem.create("event-persistence-support");
        System.out.println(actorSystem);
        eventPersistentActor = actorSystem.actorOf(Props.create(LexEventPersistor.class,ClusterMode.OFF));
        System.out.println(eventPersistentActor);
    }

    public PersistenceBackedEventTrigger() {
        super();
    }

    @Override
    protected void onTrigger(TriggerContext context) throws Exception {
        switch (Configuration.getConfiguration().getPersistenceMechanism()) {
            case "akka": {
                //TODO - use actorSelection to get and then send
                String nodeHost = Cluster.get(ClusterSystem.getInstance().getActorSystem()).selfAddress().toString();

                eventPersistentActor.tell(new ActionCommand(context.getCommands(), context.getActionAliasPath(), context.getEventName(), context.getParameters(),context.getSessionid(),nodeHost), ActorRef.noSender());
            }

            // others on demand here - as of now its akka only
        }
    }

}

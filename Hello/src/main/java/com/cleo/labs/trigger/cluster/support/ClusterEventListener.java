package com.cleo.labs.trigger.cluster.support;

import akka.actor.UntypedActor;
import akka.cluster.Cluster;
import akka.cluster.ClusterEvent;
import akka.cluster.ClusterEvent.*;
import akka.event.Logging;
import akka.event.LoggingAdapter;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by sbelur on 06/10/15.
 */
public class ClusterEventListener extends UntypedActor {

    LoggingAdapter log = Logging.getLogger(getContext().system(), this);
    Cluster cluster = Cluster.get(getContext().system());

    private Map<String, Boolean> downNodes = new HashMap();


    @Override
    public void preStart() {
        cluster.subscribe(getSelf(), ClusterEvent.initialStateAsEvents(),
                MemberEvent.class, UnreachableMember.class);
    }


    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof MemberUp) {
            MemberUp mUp = (MemberUp) message;
            downNodes.remove(mUp.member().address().toString());
            log.info("unregistered " + mUp.member().address().toString() + " as member down");


        } else if (message instanceof UnreachableMember) {
            UnreachableMember mUnreachable = (UnreachableMember) message;
            log.info("ClusterEventListener - Member detected as unreachable: {}", mUnreachable.member());

        } else if (message instanceof MemberRemoved) {
            MemberRemoved mRemoved = (MemberRemoved) message;
            downNodes.put(mRemoved.member().address().toString(), true);
            log.info("registered " + mRemoved.member().address().toString() + " as member down");

        } else if (message instanceof MemberEvent) {
            // ignore

        } else if (message instanceof NodeStatus) {
            NodeStatus nodeStatus = (NodeStatus) message;
            getSender().tell(isNodeDown(nodeStatus.getHost()), getSelf());
        } else {
            unhandled(message);
        }

    }


    //re-subscribe when restart
    @Override
    public void postStop() {
        cluster.unsubscribe(getSelf());
    }


    private boolean isNodeDown(String address) {
        return downNodes.containsKey(address);
    }

}

package com.cleo.labs.trigger.cluster.support;

import java.io.Serializable;

/**
 * Created by sbelur on 11/10/15.
 */
public final class NodeStatus implements Serializable {

    private static final long serialVersionUID = 1L;


    private final String host;

    public NodeStatus(String host) {
        this.host = host;
    }

    public String getHost() {
        return host;
    }
}

package com.cleo.labs.trigger.persist.akka.events;

import java.io.Serializable;

/**
 * Created by sbelur on 20/09/15.
 */
public final class ConfirmationLexEvent implements Serializable{

    private static final long serialVersionUID = 1L;

    private final Object eventDeliveryId;

    private final String sessionId;
    private final Long start;
    private final Long end;

    public ConfirmationLexEvent(Object eventDeliveryId, String sessionId, Long start, Long end) {
        this.eventDeliveryId = eventDeliveryId;
        this.sessionId = sessionId;
        this.start = start;
        this.end = end;
    }

    public Long getStart() {
        return start;
    }

    public Long getEnd() {
        return end;
    }

    public Object getEventDeliveryId() {
        return eventDeliveryId;
    }

    public String getSessionId() {
        return sessionId;
    }
}

package com.cleo.labs.trigger.persist.akka.messages;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Created by sbelur on 20/09/15.
 */
public class ActionCommand  implements Serializable{

    private static final long serialVersionUID = 1L;
    private final String[] commands;
    private final String[] aliasPath;
    private final String eventName;
    private final String[] parameters;
    private final String sessionid;
    private final String nodeHost;


    public ActionCommand(String[] commands, String[] actionAliasPath, String eventName, String[] parameters, String sessionid,String nodeHost) {
        this.commands = commands;
        this.aliasPath = actionAliasPath;
        this.eventName = eventName;
        this.parameters = parameters;
        this.sessionid = sessionid;
        this.nodeHost = nodeHost;
    }

    public String getNodeHost() {
        return nodeHost;
    }

    public String[] commands(){
        return commands;
    }
    public String[] aliasPath(){
        return aliasPath;
    }


    public String getEventName() {
        return eventName;
    }

    public String[] getParameters() {
        return parameters;
    }

    public String getSessionid() {
        return sessionid;
    }

    @Override
    public String toString() {
        return eventName + " , "+ Arrays.asList(parameters); // TODO store as list itself.
    }
}



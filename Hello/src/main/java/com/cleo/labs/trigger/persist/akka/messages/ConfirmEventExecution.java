package com.cleo.labs.trigger.persist.akka.messages;

import java.io.Serializable;

/**
 * Created by sbelur on 20/09/15.
 */
public class ConfirmEventExecution implements Serializable{

    private static final long serialVersionUID = 1L;


    private final Object deliveryId;
    private final Long start;
    private final Long end;

    private final String sessionId;

    public ConfirmEventExecution(Long deliveryId, String sessionId) {
        this.deliveryId = deliveryId;
        this.sessionId = sessionId;
        this.start = null;
        this.end = null;
    }
    public ConfirmEventExecution(Object deliveryId, Long start,Long end) {
        this.deliveryId = deliveryId;
        this.start = start;
        this.end = end;
        this.sessionId = null;
    }


    public Long getStart() {
        return start;
    }

    public Long getEnd() {
        return end;
    }

    public Object getDeliveryId() {
        return deliveryId;
    }


    public String getSessionId() {
        return sessionId;
    }
}

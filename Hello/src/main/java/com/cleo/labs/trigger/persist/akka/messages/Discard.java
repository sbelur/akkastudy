package com.cleo.labs.trigger.persist.akka.messages;

import java.io.Serializable;

/**
 * Created by sbelur on 11/10/15.
 */
public final class Discard implements Serializable {

    private static final long serialVersionUID = 1L;
    private final int id;

    public Discard(int id){

        this.id = id;
    }

    public int getId() {
        return id;
    }
}

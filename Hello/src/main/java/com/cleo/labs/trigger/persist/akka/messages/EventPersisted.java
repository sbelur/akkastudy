package com.cleo.labs.trigger.persist.akka.messages;

/**
 * Created by sbelur on 10/10/15.
 */
public final class EventPersisted {

    private final boolean done;

    public EventPersisted(boolean done){

        this.done = done;
    }

    public boolean isDone() {
        return done;
    }
}

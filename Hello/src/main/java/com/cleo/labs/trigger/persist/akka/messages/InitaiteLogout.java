package com.cleo.labs.trigger.persist.akka.messages;

import java.io.Serializable;

/**
 * Created by sbelur on 01/10/15.
 */
public final class InitaiteLogout implements Serializable{

    private static final long serialVersionUID = 1L;

    private final String sessionid;

    public InitaiteLogout(String sessionid) {
        this.sessionid = sessionid;
    }

    public String getSessionid() {
        return sessionid;
    }
}

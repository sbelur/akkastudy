package com.cleo.labs.trigger.persist.akka.persistor;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.cluster.routing.ClusterRouterPool;
import akka.cluster.routing.ClusterRouterPoolSettings;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.routing.ConsistentHashingPool;
import akka.routing.ConsistentHashingRouter;
import com.cleo.labs.trigger.cluster.support.ClusterMode;
import com.cleo.labs.trigger.persist.akka.messages.ActionCommand;
import com.cleo.labs.trigger.persist.akka.messages.EventPersisted;

/**
 * Created by sbelur on 10/10/15.
 */
public class ClusterPersistorDelegator extends UntypedActor {

    private LoggingAdapter log = Logging.getLogger(getContext().system(), this);
    private ActorRef sessionRef;


    @Override
    public void preStart() throws Exception {
        super.preStart();

        final int totalInstances = getContext().system().settings().config().getInt("noOfTopics");
        int maxInstancesPerNode = totalInstances/2;

        sessionRef = getContext().actorOf(
                new ClusterRouterPool(new ConsistentHashingPool(0).withHashMapper(new ConsistentHashingRouter.ConsistentHashMapper() {

                    @Override
                    public Object hashKey(Object message) {
                        if (message instanceof ActionCommand) {
                            String sessionid = ((ActionCommand) message).getSessionid();
                            long hashingId = sessionid.hashCode()%totalInstances;
/*
                            log.info("Dispatching to persistor for hash id " + hashingId
                                    + " for session " + sessionid + " msg - "+message);
*/
                            return hashingId;
                        }
                        return null;
                    }
                }),
                        new ClusterRouterPoolSettings(totalInstances, maxInstancesPerNode,
                                true, "")).props(Props
                        .create(LexEventPersistor.class, ClusterMode.ON)), "persistorRouter");

    }

    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof ActionCommand) {
            sessionRef.tell(message, getSelf());
        } else if (message instanceof EventPersisted) {
            //TODO - integrate w/ 200 OK response.
        }
        // else TODO - add the check for persistence failure here.

        else {
            unhandled(message);
        }
    }
}

package com.cleo.labs.trigger.persist.akka.persistor;

import akka.actor.*;
import akka.cluster.routing.ClusterRouterPool;
import akka.cluster.routing.ClusterRouterPoolSettings;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.japi.Function;
import akka.japi.Procedure;
import akka.persistence.RecoveryCompleted;
import akka.persistence.RecoveryFailure;
import akka.persistence.UntypedPersistentActor;
import akka.persistence.UntypedPersistentActorWithAtLeastOnceDelivery;
import akka.routing.*;
import com.cleo.labs.trigger.Configuration;
import com.cleo.labs.trigger.cluster.support.ClusterMode;
import com.cleo.labs.trigger.persist.akka.events.ConfirmationLexEvent;
import com.cleo.labs.trigger.persist.akka.events.IdentifiableLexEvent;
import com.cleo.labs.trigger.persist.akka.events.LexEvent;
import com.cleo.labs.trigger.persist.akka.messages.ActionCommand;
import com.cleo.labs.trigger.persist.akka.messages.ConfirmEventExecution;
import com.cleo.labs.trigger.persist.akka.messages.Discard;
import com.cleo.labs.trigger.persist.akka.messages.InitaiteLogout;
import com.cleo.labs.trigger.persist.akka.worker.EventExecutor;
import com.cleo.labs.trigger.persist.akka.worker.SessionEventExecutor;
import com.cleo.util.logger.XMLLogElement;
import research.*;
import scala.Option;
import scala.concurrent.duration.FiniteDuration;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import scala.concurrent.Future;
import static akka.dispatch.Futures.future;

/**
 * Created by sbelur on 20/09/15.
 * <pre>
 *   Supports atleast once delivery guarantee mechanism by persisting events
 *
 *   At a high level, persists events for the command received. Once persisted,
 *   the event is dispatched to a worker actor. Delivery is deemed confirmed,
 *   when the worker actor confirms the same delivery Id sent to it.
 *
 *   If the worker crashes after persistence, replay is triggered when this
 *   Actor (Persistor) recovers back.
 *
 * </pre>
 */
public class EventPersistor extends UntypedPersistentActor {

    private final ClusterMode clusterMode;
    private final boolean perftest;
    private ActorRef routerRef;
    private ActorRef sessionRef;
    private boolean firstMsg = false;

    // During recovery for crash .
    private Map<String, Cancellable> forceLogout = new HashMap<>();

    private LoggingAdapter log = Logging.getLogger(getContext().system(), this);

    //private Random r = new Random();

    int totalInstances;


    @Override
    public String persistenceId() {
        String pId = getSelf().path().parent().name() + "-" + getSelf().path().name();
        return pId;
    }

    public EventPersistor(ClusterMode clusterMode) {
        this.clusterMode = clusterMode;
        perftest = false;
    }

    public EventPersistor(boolean perftest) {
        this.perftest = perftest;
        clusterMode = ClusterMode.OFF;

    }

    public EventPersistor() {
        this.perftest = true;
        clusterMode = ClusterMode.OFF;
    }


    @Override
    public void preStart() throws Exception {
        super.preStart();



        //TODO - externalize this


        if (perftest) {
            routerRef = getContext().actorOf(new BalancingPool(Runtime.getRuntime().availableProcessors()).props(Props.create(WorkerActor.class)),
                    "eventExecutorRouter");
            System.out.println("routerRef *** " + routerRef);
        } else {
            if (clusterMode == ClusterMode.OFF) {
                initRoutersForLocalMode();

            } else {
                initRoutersForClusterAwareness();
                //TODO build session ref for cluster....
            }
        }

        //log.info("Started a persistor " + getSelf().path());

    }


    private void initRoutersForClusterAwareness() {

        // TODO: Externalize config in .conf file.
        // TODO : Do we merge these routers ?

        final int totalInstances = 200;
        int maxInstancesPerNode = 100;
        boolean allowLocalRoutees = true;
        routerRef = getContext().actorOf(
                new ClusterRouterPool(new RoundRobinPool(0),
                        new ClusterRouterPoolSettings(totalInstances, maxInstancesPerNode,
                                allowLocalRoutees, "")).props(Props
                        .create(EventExecutor.class)), "clusterworkerRouter");

        sessionRef = getContext().actorOf(
                new ClusterRouterPool(new ConsistentHashingPool(0).withHashMapper(new ConsistentHashingRouter.ConsistentHashMapper() {

                    @Override
                    public Object hashKey(Object message) {
                        if (message instanceof IdentifiableLexEvent) {
                            String sessionid = ((IdentifiableLexEvent) message).getLexEvent().getSessionid();
                            long hashingId = sessionid.hashCode() % totalInstances;
                            return hashingId;
                        } else if (message instanceof InitaiteLogout) {

                            String sessionid = ((InitaiteLogout) message).getSessionid();
                            long hashingId = sessionid.hashCode() % totalInstances;
                            return hashingId;
                        }
                        return null;
                    }
                }),
                        new ClusterRouterPoolSettings(totalInstances, maxInstancesPerNode,
                                allowLocalRoutees, "")).props(Props
                        .create(SessionEventExecutor.class)), "clustersessionexecutorrouter");

        //sessionRef.tell(CurrentRoutees.getInstance(), getSelf());
    }

    private void initRoutersForLocalMode() {
        routerRef = getContext().actorOf(new SmallestMailboxPool(500).props(Props.create(EventExecutor.class)),
                "eventExecutorRouter");

        sessionRef = getContext().actorOf(new ConsistentHashingPool(500).withHashMapper(new ConsistentHashingRouter.ConsistentHashMapper() {

            @Override
            public Object hashKey(Object message) {
                if (message instanceof IdentifiableLexEvent) {
                    String hashingId = ((IdentifiableLexEvent) message).getLexEvent().getSessionid();
                    return hashingId;
                }
                return null;
            }
        }).props(Props.create(SessionEventExecutor.class)), "sessionexecutorrouter");
    }

    @Override
    public void onReceiveRecover(Object event) throws Exception {

        if (event instanceof RecoveryFailure) {
            log.error(((RecoveryFailure) event).cause(), "Failed to recover properly ");
        } else if (!(event instanceof RecoveryCompleted)) {
            //log.info("In recover call for event " + event);

            if (event instanceof String) {
                //log.info("Found "+event);
                /*deliver(routerRef.path(), (Long deliveryId) -> {
                    PersTestEvent persTestEvent = new PersTestEvent((String) event, -1L, -1L);
                    persTestEvent.setDeliveryId(deliveryId);
                    return persTestEvent;
                });*/
                return;
            } else if (event instanceof LexEvent) {
                LexEvent anEvent = (LexEvent) event;
                String aSID = anEvent.getSessionid();
                String type = anEvent.getEventName();
                boolean logout = Configuration.Event.logout.name().equalsIgnoreCase(type);
                boolean login = Configuration.Event.login.name().equalsIgnoreCase(type);
                if (!logout && !login) {  // NOTE: Right now non-login events are cached in session for batch action.
                    if (forceLogout.get(aSID) == null) {
                        int forceLogOutAfter = getContext().system().settings().config().getInt("session.force.timeout.in.seconds");
                        ActorRef sender = getSender();
                        ActorRef selfActor = getSelf();
                        Cancellable cancellable = getContext().system().scheduler().scheduleOnce
                                (new FiniteDuration(forceLogOutAfter, TimeUnit.SECONDS), selfActor, new InitaiteLogout(aSID), getContext().system().dispatcher(), sender);
                        forceLogout.put(aSID, cancellable);
                    }
                } else {
                    if (logout) {
                        Cancellable cancellable = forceLogout.remove(aSID);
                        if (cancellable != null) {
                            cancellable.cancel();
                        }
                    }
                }

            } else if (event instanceof ConfirmationLexEvent) {
                ConfirmationLexEvent cle = (ConfirmationLexEvent) event;
                if (cle.getSessionId() != null) {
                    Cancellable cancellable = forceLogout.remove(cle.getSessionId());
                    if (cancellable != null) {
                        cancellable.cancel();
                    }
                }

            }
            handleEvent(event);
        }
        else if(event instanceof RecoveryCompleted){
            ActorSelection sel = getContext().system().actorSelection("/user/tracker");
            sel.tell("start", getSelf());
        }

    }



    @Override
    public void onReceiveCommand(Object message) throws Exception {
        //System.out.println("in ep "+message);
         if (message instanceof SampleMessage) {
            XMLLogElement xml = ((SampleMessage) message).getElement();
            String evt = Events.getElement(xml);
            //long startp = System.currentTimeMillis();
            persistAsync(evt, new Procedure<String>() {

                @Override
                public void apply(String param) throws Exception {
                    //getSender().tell(Boolean.TRUE, getSelf());
                   /* long pt = System.currentTimeMillis() - startp;
                    if(pt > 1)
                        System.out.println("time to persist "+ pt);*/
                    PersTestEvent persTestEvent = new PersTestEvent(evt, ((SampleMessage) message).getStart(), ((SampleMessage) message).getStartInMs());
                    persTestEvent.setTimeSentToWorker(System.currentTimeMillis());
                    future(new Computation(persTestEvent, getSelf()), AKKA.getSystem().dispatcher());

                }
            });

        } else if (message instanceof ConfirmEventExecution) {
            ConfirmEventExecution confirmMsg = (ConfirmEventExecution) message;
            //long s=System.currentTimeMillis();
            StringBuilder  builder = new StringBuilder("a=1,b=2,c=3,d=4,e=5,a=1,b=2,c=3,d=4,e=5,a=1,b=2,c=3,d=4,e=5,a=1,b=2,c=3,d=4,e=5,a=1,b=2,c=3,d=4,e=5\n");
            persistAsync(builder.toString(), new Procedure<String>() {
                public void apply(String evt) {
                    //onEventPersisted(evt);
                }
            });
        } else if (message instanceof InitaiteLogout) {
            log.info("***** InitaiteLogout msg found !!!\n\n");
            InitaiteLogout il = (InitaiteLogout) message;
            String sessionid = il.getSessionid();
            if (forceLogout.containsKey(sessionid)) {
                forceLogout.remove(sessionid);
                log.info("Sending a forced logout event for session " + sessionid);
                handleEvent(il);
            }

        } else {
            if (!(message instanceof Discard)) {
                log.info("\n\n**** Not handling message **** " + message + "\n\n");
            }
            unhandled(message);
        }
    }



    private void onEventPersisted(final Object event) {
        handleEvent(event);
    }

    private void handleEvent(Object event) {
        if (event instanceof ConfirmationLexEvent) {
            final ConfirmationLexEvent evt = (ConfirmationLexEvent) event;
            //confirmDelivery((Long)evt.getEventDeliveryId());

          /*  //TODO - remove this post poc
            if (System.getProperty("persist") != null) {
                ActorSelection sel = getContext().system().actorSelection("/user/tracker");
                sel.tell(new WorkDone(((ConfirmationLexEvent) event).getStart(), ((ConfirmationLexEvent) event).getEnd()), getSelf());
                //deleteMessage(((ConfirmationLexEvent) event).getEventDeliveryId());
            }*/
        } else if (event instanceof InitaiteLogout) {
            InitaiteLogout il = (InitaiteLogout) event;
            log.info("Sending a logout to session router " + il.getSessionid());
            sessionRef.tell(il, getSelf());
        }
    }





    private static class Computation implements Callable<Void> {

        private final PersTestEvent paramObject;
        private final ActorRef sender;


        public Computation(PersTestEvent persTestEvent, ActorRef sender) {
            this.paramObject = persTestEvent;
            this.sender = sender;
        }

        public Void call() throws Exception {
            long s = System.currentTimeMillis();
            long d = paramObject.getTimeSentToWorker();
            /*long tm = s - d;
            if(tm > 100){
                System.out.println("to future "+ tm);
            }*/
            long end = System.nanoTime();
            int i;
            for (i = 0; i < 1000000; i++) {
                for (int j = 0; j < 10000; j++) {
                    float comlexCal = 676764 ^ 676 ^ 676 * 898 / 67 * 4 ^ 7 ^ 7 * 89;
                    comlexCal = 676764 ^ 676 ^ 676 * 898 / 67 * 4 ^ 7 ^ 7 * 89;
                    for (int k = 0; k < 1000; k++) {
                        comlexCal = 676764 ^ 676 ^ 676 * 898 / 67 * 4 ^ 7 ^ 7 * 89;
                        comlexCal = 676764 ^ 676 ^ 676 * 898 / 67 * 4 ^ 7 ^ 7 * 89;
                    }
                }
            }
            sender.tell(new ConfirmEventExecution(paramObject.getDeliveryIdStr(), paramObject.getStart(), end), ActorRef.noSender());

            ActorSelection sel = AKKA.getSystem().actorSelection("/user/tracker");
            sel.tell(new WorkDone(((PersTestEvent) paramObject).getStart(), end), ActorRef.noSender());
            //deleteMessage(((ConfirmationLexEvent) event).getEventDeliveryId());
            return null;
        }
    }

}


package com.cleo.labs.trigger.persist.akka.persistor;

import akka.actor.*;
import akka.cluster.routing.ClusterRouterPool;
import akka.cluster.routing.ClusterRouterPoolSettings;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.japi.Function;
import akka.japi.Procedure;
import akka.persistence.RecoveryCompleted;
import akka.persistence.RecoveryFailure;
import akka.persistence.UntypedPersistentActorWithAtLeastOnceDelivery;
import akka.routing.*;
import com.cleo.labs.trigger.Configuration;
import com.cleo.labs.trigger.cluster.support.ClusterMode;
import com.cleo.labs.trigger.persist.akka.events.ConfirmationLexEvent;
import com.cleo.labs.trigger.persist.akka.events.IdentifiableLexEvent;
import com.cleo.labs.trigger.persist.akka.events.LexEvent;
import com.cleo.labs.trigger.persist.akka.messages.ActionCommand;
import com.cleo.labs.trigger.persist.akka.messages.ConfirmEventExecution;
import com.cleo.labs.trigger.persist.akka.messages.Discard;
import com.cleo.labs.trigger.persist.akka.messages.InitaiteLogout;
import com.cleo.labs.trigger.persist.akka.worker.EventExecutor;
import com.cleo.labs.trigger.persist.akka.worker.SessionEventExecutor;
import com.cleo.util.logger.XMLLogElement;
import research.*;
import scala.Function1;
import scala.Option;
import scala.concurrent.duration.FiniteDuration;
import scala.runtime.BoxedUnit;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by sbelur on 20/09/15.
 * <pre>
 *   Supports atleast once delivery guarantee mechanism by persisting events
 *
 *   At a high level, persists events for the command received. Once persisted,
 *   the event is dispatched to a worker actor. Delivery is deemed confirmed,
 *   when the worker actor confirms the same delivery Id sent to it.
 *
 *   If the worker crashes after persistence, replay is triggered when this
 *   Actor (Persistor) recovers back.
 *
 * </pre>
 */
public class LexEventPersistor extends UntypedPersistentActorWithAtLeastOnceDelivery {

    private final ClusterMode clusterMode;
    private final boolean perftest;
    private ActorRef routerRef;
    private ActorRef sessionRef;
    private boolean firstMsg = false;

    // During recovery for crash .
    private Map<String, Cancellable> forceLogout = new HashMap<>();

    private LoggingAdapter log = Logging.getLogger(getContext().system(), this);

    //private Random r = new Random();

    int totalInstances;


    public LexEventPersistor(ClusterMode clusterMode) {
        this.clusterMode = clusterMode;
        perftest = false;
    }

    public LexEventPersistor(boolean perftest) {
        this.perftest = perftest;
        clusterMode = ClusterMode.OFF;

    }

    public LexEventPersistor() {
        this.perftest = true;
        clusterMode = ClusterMode.OFF;
    }


    @Override
    public void preStart() throws Exception {
        super.preStart();



        //TODO - externalize this


        if (perftest) {
            routerRef = getContext().actorOf(new BalancingPool(Runtime.getRuntime().availableProcessors()).props(Props.create(WorkerActor.class)),
                    "eventExecutorRouter");
            System.out.println("routerRef *** "+routerRef);
        } else {
            if (clusterMode == ClusterMode.OFF) {
                initRoutersForLocalMode();

            } else {
                initRoutersForClusterAwareness();
                //TODO build session ref for cluster....
            }
        }

        //log.info("Started a persistor " + getSelf().path());

    }


    private void initRoutersForClusterAwareness() {

        // TODO: Externalize config in .conf file.
        // TODO : Do we merge these routers ?

        final int totalInstances = 200;
        int maxInstancesPerNode = 100;
        boolean allowLocalRoutees = true;
        routerRef = getContext().actorOf(
                new ClusterRouterPool(new RoundRobinPool(0),
                        new ClusterRouterPoolSettings(totalInstances, maxInstancesPerNode,
                                allowLocalRoutees, "")).props(Props
                        .create(EventExecutor.class)), "clusterworkerRouter");

        sessionRef = getContext().actorOf(
                new ClusterRouterPool(new ConsistentHashingPool(0).withHashMapper(new ConsistentHashingRouter.ConsistentHashMapper() {

                    @Override
                    public Object hashKey(Object message) {
                        if (message instanceof IdentifiableLexEvent) {
                            String sessionid = ((IdentifiableLexEvent) message).getLexEvent().getSessionid();
                            long hashingId = sessionid.hashCode() % totalInstances;
                            return hashingId;
                        } else if (message instanceof InitaiteLogout) {

                            String sessionid = ((InitaiteLogout) message).getSessionid();
                            long hashingId = sessionid.hashCode() % totalInstances;
                            return hashingId;
                        }
                        return null;
                    }
                }),
                        new ClusterRouterPoolSettings(totalInstances, maxInstancesPerNode,
                                allowLocalRoutees, "")).props(Props
                        .create(SessionEventExecutor.class)), "clustersessionexecutorrouter");

        //sessionRef.tell(CurrentRoutees.getInstance(), getSelf());
    }

    private void initRoutersForLocalMode() {
        routerRef = getContext().actorOf(new SmallestMailboxPool(500).props(Props.create(EventExecutor.class)),
                "eventExecutorRouter");

        sessionRef = getContext().actorOf(new ConsistentHashingPool(500).withHashMapper(new ConsistentHashingRouter.ConsistentHashMapper() {

            @Override
            public Object hashKey(Object message) {
                if (message instanceof IdentifiableLexEvent) {
                    String hashingId = ((IdentifiableLexEvent) message).getLexEvent().getSessionid();
                    return hashingId;
                }
                return null;
            }
        }).props(Props.create(SessionEventExecutor.class)), "sessionexecutorrouter");
    }

    @Override
    public void onReceiveRecover(Object event) throws Exception {

        if (event instanceof RecoveryFailure) {
            log.error(((RecoveryFailure) event).cause(), "Failed to recover properly ");
        } else if (!(event instanceof RecoveryCompleted)) {
            //log.info("In recover call for event " + event);

            if (event instanceof String) {
                //log.info("Found "+event);
                deliver(routerRef.path(), (Long deliveryId) -> {
                    PersTestEvent persTestEvent = new PersTestEvent((String) event, -1L, -1L);
                    persTestEvent.setDeliveryId(deliveryId);
                    return persTestEvent;
                });
                return;
            } else if (event instanceof LexEvent) {
                LexEvent anEvent = (LexEvent) event;
                String aSID = anEvent.getSessionid();
                String type = anEvent.getEventName();
                boolean logout = Configuration.Event.logout.name().equalsIgnoreCase(type);
                boolean login = Configuration.Event.login.name().equalsIgnoreCase(type);
                if (!logout && !login) {  // NOTE: Right now non-login events are cached in session for batch action.
                    if (forceLogout.get(aSID) == null) {
                        int forceLogOutAfter = getContext().system().settings().config().getInt("session.force.timeout.in.seconds");
                        ActorRef sender = getSender();
                        ActorRef selfActor = getSelf();
                        Cancellable cancellable = getContext().system().scheduler().scheduleOnce
                                (new FiniteDuration(forceLogOutAfter, TimeUnit.SECONDS), selfActor, new InitaiteLogout(aSID), getContext().system().dispatcher(), sender);
                        forceLogout.put(aSID, cancellable);
                    }
                } else {
                    if (logout) {
                        Cancellable cancellable = forceLogout.remove(aSID);
                        if (cancellable != null) {
                            cancellable.cancel();
                        }
                    }
                }

            } else if (event instanceof ConfirmationLexEvent) {
                ConfirmationLexEvent cle = (ConfirmationLexEvent) event;
                if (cle.getSessionId() != null) {
                    Cancellable cancellable = forceLogout.remove(cle.getSessionId());
                    if (cancellable != null) {
                        cancellable.cancel();
                    }
                }

            }
            handleEvent(event);
        }
        else if(event instanceof RecoveryCompleted){
            ActorSelection sel = getContext().system().actorSelection("/user/tracker");
            sel.tell("start", getSelf());
        }

    }

    // Temporary - Just to ensure that crash test passes.
    @Override
    public FiniteDuration redeliverInterval() {
        return new FiniteDuration(600, TimeUnit.SECONDS);
    } // todo-revisit

    @Override
    public void onReceiveCommand(Object message) throws Exception {

        if ("reset".equals(message)) {
            firstMsg = false;
        } else if (message instanceof SampleMessage) {
            XMLLogElement xml = ((SampleMessage) message).getElement();
           /*if (!firstMsg) {
                firstMsg = true;
                ActorSelection sel = getContext().system().actorSelection("/user/tracker");
                sel.tell("start", getSelf());
                //System.out.println("Sent start to tracker");
            }*/
            String evt = Events.getElement(xml);
            //long startp = System.currentTimeMillis();
            persistAsync(evt, new Procedure<String>() {

                @Override
                public void apply(String param) throws Exception {
                    //getSender().tell(Boolean.TRUE, getSelf());
                   /* long pt = System.currentTimeMillis() - startp;
                    if(pt > 1)
                        System.out.println("time to persist "+ pt);*/
                    ActorPath destination = routerRef.path();
                    PersTestEvent persTestEvent = new PersTestEvent(evt, ((SampleMessage) message).getStart(), ((SampleMessage) message).getStartInMs());
                    persTestEvent.setTimeSentToWorker(System.currentTimeMillis());
                    deliver(destination, (Long deliveryId) -> {
                        persTestEvent.setDeliveryId(deliveryId);
                        return persTestEvent;
                    });
                }
            });

        } else if (message instanceof ActionCommand) {
            ActionCommand actionCommand = (ActionCommand) message;
            log.info("Persisting in persistor " + getSelf().path());
            persistAsync(new LexEvent(actionCommand.commands(), actionCommand.aliasPath(), actionCommand.getEventName(), actionCommand.getParameters(), actionCommand.getSessionid(), actionCommand.getNodeHost()), new Procedure<LexEvent>() {
                public void apply(LexEvent evt) {
                    onEventPersisted(evt);
                }
            });
        } else if (message instanceof ConfirmEventExecution) {
            ConfirmEventExecution confirmMsg = (ConfirmEventExecution) message;
            //long s=System.currentTimeMillis();
            persistAsync(new ConfirmationLexEvent(confirmMsg.getDeliveryId(), confirmMsg.getSessionId(), ((ConfirmEventExecution) message).getStart(), ((ConfirmEventExecution) message).getEnd()), new Procedure<ConfirmationLexEvent>() {
                public void apply(ConfirmationLexEvent evt) {
                    onEventPersisted(evt);
                }
            });
        } else if (message instanceof InitaiteLogout) {
            log.info("***** InitaiteLogout msg found !!!\n\n");
            InitaiteLogout il = (InitaiteLogout) message;
            String sessionid = il.getSessionid();
            if (forceLogout.containsKey(sessionid)) {
                forceLogout.remove(sessionid);
                log.info("Sending a forced logout event for session " + sessionid);
                handleEvent(il);
            }

        } else {
            if (!(message instanceof Discard)) {
                log.info("\n\n**** Not handling message **** " + message + "\n\n");
            }
            unhandled(message);
        }
    }

    @Override
    public String persistenceId() {
        String pId = getSelf().path().parent().name() + "-" + getSelf().path().name();
        return pId;
    }


    private void onEventPersisted(final Object event) {
        handleEvent(event);
    }

    private void handleEvent(Object event) {
        if (event instanceof LexEvent) {
            ActorPath destination = routerRef.path();
            final LexEvent evt = (LexEvent) event;
            List<String> params = Arrays.asList(evt.getParameters());
            int mbIndex = params.indexOf("mailbox");
            if (mbIndex != -1) {
                String mb = params.get(mbIndex + 1);
                //TEMPORARY means to identify session based handling for a user
                // Right now all events for this user will be batched.
                if ("batch_ftp".equals(mb)) {
                    destination = sessionRef.path();
                }
            }
            deliver(destination, new Function<Long, Object>() {

                @Override
                public Object apply(Long deliveryId) throws Exception {
                    return new IdentifiableLexEvent(deliveryId, evt);
                }
            });

        } else if (event instanceof ConfirmationLexEvent) {
            final ConfirmationLexEvent evt = (ConfirmationLexEvent) event;
            confirmDelivery((Long)evt.getEventDeliveryId());

          /*  //TODO - remove this post poc
            if (System.getProperty("persist") != null) {
                ActorSelection sel = getContext().system().actorSelection("/user/tracker");
                sel.tell(new WorkDone(((ConfirmationLexEvent) event).getStart(), ((ConfirmationLexEvent) event).getEnd()), getSelf());
                //deleteMessage(((ConfirmationLexEvent) event).getEventDeliveryId());
            }*/
        } else if (event instanceof InitaiteLogout) {
            InitaiteLogout il = (InitaiteLogout) event;
            log.info("Sending a logout to session router " + il.getSessionid());
            sessionRef.tell(il, getSelf());
        }
    }


    @Override
    public void postStop() {
        //log.info("Stopping a persistor " + getSelf().path());
        super.postStop();
    }


    @Override
    public void preRestart(Throwable reason, Option<Object> message) {
        //log.info("ReStarting a persistor " + getSelf().path());
        super.preRestart(reason, message);
    }

    @Override
    public void postRestart(Throwable reason) throws Exception {
        //log.info("Post - ReStarting a persistor " + getSelf().path());
        super.postRestart(reason);
    }


    @Override
    public int maxUnconfirmedMessages() {
        return Integer.MAX_VALUE;
    }
}


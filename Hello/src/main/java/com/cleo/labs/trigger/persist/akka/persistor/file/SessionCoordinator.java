package com.cleo.labs.trigger.persist.akka.persistor.file;

import akka.actor.*;
import akka.pattern.Patterns;
import research.AKKA;
import research.SampleMessage;
import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.FiniteDuration;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

/**
 * Created by sbelur on 23/10/15.
 */
public class SessionCoordinator {

    private Map<Integer, ActorRef> sessionPersistors = new HashMap<>();

    private ThreadLocalRandom r = ThreadLocalRandom.current();

    int no = 2 * Runtime.getRuntime().availableProcessors();

    public SessionCoordinator() {
        for (int z = 0; z < no; z++) {
            ActorRef actorRef = AKKA.getSystem().actorOf(Props.create(FilePersistor.class, "" + z), "store" + z);
            sessionPersistors.put(z, actorRef);
        }
    }

    public void onReceive(SampleMessage message) throws Exception {

        // simulate FTP thread.
        //FTP_POOL.execute(() -> {
            int sid = r.nextInt(100) % no;
            ActorRef ref = sessionPersistors.get(sid);
            ref.tell(message,ActorRef.noSender());
            /*Future<Object> res = Patterns.ask(ref, message, 5000L);
            try {
                Await.result(res, new FiniteDuration(5000L, TimeUnit.SECONDS));
                //send res to client
            } catch (Exception e) {
                // send timeout
            }*/
        //});
    }

    //private static final ExecutorService FTP_POOL = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors()*2);

}

package com.cleo.labs.trigger.persist.akka.worker;

import akka.actor.ActorRef;
import akka.actor.UntypedActor;
import akka.cluster.Cluster;
import akka.dispatch.OnComplete;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.pattern.Patterns;
import akka.util.Timeout;
import com.cleo.labs.trigger.Configuration;
import com.cleo.labs.trigger.ExecutionUtil;
import com.cleo.labs.trigger.cluster.support.ClusterSystem;
import com.cleo.labs.trigger.cluster.support.NodeStatus;
import com.cleo.labs.trigger.persist.akka.events.IdentifiableLexEvent;
import com.cleo.labs.trigger.persist.akka.messages.ConfirmEventExecution;
import com.cleo.labs.trigger.persist.akka.messages.InitaiteLogout;
import com.cleo.labs.trigger.session.SessionEvent;
import com.cleo.lexicom.external.ILexiCom;
import com.cleo.lexicom.external.LexiComFactory;
import scala.concurrent.Future;
import scala.concurrent.duration.FiniteDuration;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by sbelur on 20/09/15.
 * <pre>
 * worker which groups events for a session in memory as of now till it sees a logout action.
 * This is very naive implementation for POC concept ONLY. Does not take any memory considerations for now.
 *
 * If there is a failure / crash, the persistor will replay all events. So this relies on the reliability of
 * persistor.
 *
 * </pre>
 */
public class SessionEventExecutor extends UntypedActor {

    private LoggingAdapter log = Logging.getLogger(getContext().system(), this);

    // Map(sessionid -> evt container) //TODO : check for optimizing the storage
    private Map<String, SessionEvent> sessionEvents = new HashMap<>();


    @Override
    public void preStart() throws Exception {
        //log.info("Created session worker actor " + getSelf().path());
        super.preStart();
    }


    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof IdentifiableLexEvent) {
            final IdentifiableLexEvent msg = (IdentifiableLexEvent) message;

            List<String> params = Arrays.asList(msg.getLexEvent().getParameters());


            String nodeHost = Cluster.get(getContext().system()).selfAddress().toString();

            if (msg.getLexEvent().getConnectedNode() != null && !msg.getLexEvent().getConnectedNode().equals(nodeHost)) {
                ActorRef clusterListener = ClusterSystem.getInstance().getMemberListenerProxy();

                Future nodeStatusFuture = Patterns.ask(clusterListener, new NodeStatus(msg.getLexEvent().getConnectedNode()), new Timeout(1, TimeUnit.SECONDS));
                nodeStatusFuture.onComplete(new OnComplete() {
                    @Override
                    public void onComplete(Throwable failure, Object success) throws Throwable {
                        if (failure == null && Boolean.TRUE.equals(success)) {
                            log.info("Forcing a logout as the previous worker node died " + msg);
                            int forceLogOutAfter = getContext().system().settings().config().getInt("session.force.timeout.in.seconds");
                            ActorRef sender = getSender();
                            ActorRef selfActor = getSelf();
                            getContext().system().scheduler().scheduleOnce(new FiniteDuration(forceLogOutAfter, TimeUnit.SECONDS), selfActor,
                                    new InitaiteLogout(msg.getLexEvent().getSessionid()),
                                    getContext().system().dispatcher(), sender);
                        }
                    }
                }, getContext().system().dispatcher());


            }


            String sessionid = msg.getLexEvent().getSessionid();
            ActorRef selfActor = getSelf();
            log.info("In SessionExecutor - Received event for session " + sessionid + " in actor " + selfActor + " with path " + selfActor.path() + " event " + msg);

            // A temporary mechanism to show case failure handling
            // Just a sleep so that second attempt can be made successful.
            if (params.toString().contains("sleep.txt")) {
                log.info("Intentionally sleeping for 10 secs now if filename is sleep.txt to check recovery - try crashing before sleep is done");
                Thread.sleep(10000);
                log.info("Sleep over");
            }

            SessionEvent sessionEvent = sessionEvents.get(sessionid);
            if (sessionEvent == null) {
                sessionEvent = new SessionEvent();
                sessionEvents.put(sessionid, sessionEvent);
            }

            long latency = msg.getLatency();
            if (latency > 0) // File events only
            {
                log.info("File Event Latency found to be " + latency + " for parameters " + params);
            }
            boolean logout = msg.getLexEvent().getEventName().equals(Configuration.Event.logout.name());
            if (!logout) {
                handleNonLogoutEvent(msg, sessionid, sessionEvent);
            } else {
                handleLogoutEvent(msg, sessionid, sessionEvent);
            }

        } else if (message instanceof InitaiteLogout) {
            InitaiteLogout il = (InitaiteLogout) message;
            String sessionid = il.getSessionid();
            log.info("Handling a forced logout event for session " + sessionid + "\n\n");
            handleLogoutEvent(null, sessionid, sessionEvents.get(sessionid));

        } else {
            unhandled(message);
        }
    }

    private void handleNonLogoutEvent(IdentifiableLexEvent msg, String sessionid, SessionEvent sessionEvent) {
        boolean login = msg.getLexEvent().getEventName().equals(Configuration.Event.login.name());
        if (!login) {
            boolean added = sessionEvent.addToBatch(msg);
            if (added) {
                log.info("Executing command for event now  but will also track event for session id " + sessionid );
                executeCommand(msg);
            }
        } else {
            executeCommand(msg); // Execute Login command
            getSender().tell(new ConfirmEventExecution(msg.getEventDeliveryId(), sessionid), getSelf()); // ACK login event
        }
    }

    private void handleLogoutEvent(IdentifiableLexEvent logoutMsg, String sessionid, SessionEvent sessionEvent) {
       sessionEvents.remove(sessionid);
        if (sessionEvent != null && sessionEvent.get() != null && !sessionEvent.get().isEmpty()) {
            log.info("Executing session events as a batch for session " + sessionid);
            // We get a null log out msg in case of timeouts or recovery.
            if (logoutMsg != null) {
                StringBuilder files = new StringBuilder();
                for(IdentifiableLexEvent ile : sessionEvent.get()){
                    List<String> list = Arrays.asList(ile.getLexEvent().getParameters()); // TODO make params a map
                    String aFile = list.get(list.indexOf("file")+1);
                    files.append("\""+aFile+"\" ");
                }
                String[] commands = logoutMsg.getLexEvent().getCommandsToExecute();
                files.deleteCharAt(files.lastIndexOf(" "));
                String str = files.toString();
                for (int i=0; i<commands.length; i++) {
                    commands[i] = commands[i].replaceAll("%sessionfiles%", str);
                }
                logoutMsg.getLexEvent().setCommandsToExecute(commands);
                executeCommand(logoutMsg); // Execute Logout command
            }
        }
        // Ack ALL events in session
        for (IdentifiableLexEvent bm : sessionEvent.get()) {
            getSender().tell(new ConfirmEventExecution(bm.getEventDeliveryId(), sessionid), getSelf());
        }

        if(logoutMsg != null) {
            // Ack Logout event for session
            getSender().tell(new ConfirmEventExecution(logoutMsg.getEventDeliveryId(), sessionid), getSelf());
        }

    }

    private void executeCommand(IdentifiableLexEvent bm) {
        try {
            ExecutionUtil.execute(bm.getLexEvent().getCommandsToExecute(), bm.getLexEvent().getActionAliasPath(), lexicom());
        } catch (Exception e) {
            // TODO : TEMPORARY - need to find the root cause of Action Already running error - what needs to be cleanup approach?.
            log.error("Error while executing command " + e.getMessage());
        }
    }

    private ILexiCom lexicom() {
        ILexiCom lexicom = null;
        try {
            lexicom = LexiComFactory.getCurrentInstance();
        } catch (Exception ignore) {
            ignore.printStackTrace();
        }
        return lexicom;
    }
}

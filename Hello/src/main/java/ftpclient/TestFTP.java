/**
 * 
 */
package ftpclient;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;


/**
 * @author ragarwal
 *
 */
public class TestFTP {

	public static int totalCLients = 10;
	public static int dataFiles = 5000;
	static ExecutorService workerPool = Executors.newFixedThreadPool(totalCLients);
	static AtomicInteger filesSent = new AtomicInteger();
	


	public static void main(String[] args) throws Exception {
		final List<File> fileList = createDataFiles(dataFiles);
		System.out.println("sample test data created successfully.");

		List<FTPClient> ftpClientList = ftpConnection(totalCLients);
		long start = System.currentTimeMillis();
		for (FTPClient ftpClient : ftpClientList) {
				workerPool.submit(new Runnable() {
					public void run() {
						try {
							exchangeFile(fileList, ftpClient);
							ftpClient.logout();
							ftpClient.disconnect();
						}  catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				});
//			}
			//ftpClient.logout();
			//ftpClient.disconnect();
		}
		
			
		while (filesSent.get() < 3000) {
			Thread.sleep(10);
		}

		long endTime = System.currentTimeMillis();
		System.out.println("files sent  = " + filesSent.get());
		System.out.println("Time taken = " + (endTime - start));
		System.out.println("no of files = " + filesSent);
		workerPool.shutdownNow();
	}

	public static List<File> createDataFiles(int count) throws Exception{
		List<File> fileList = new ArrayList<File>();
		for(int i=0;i<count;i++) {
			URL resource = TestFTP.class.getResource(".");
			File file = new File(resource.getFile()+"test.data"+count);
			FileOutputStream stream = new FileOutputStream(file);
			stream.write("simple test data".getBytes());
			stream.close();
			fileList.add(file);
		}
		return fileList;
	}

	private static void exchangeFile(List<File> fileList, FTPClient ftpClient) {

		for (File file : fileList) {
			InputStream stream = TestFTP.class.getResourceAsStream(file
					.getName());

			try {
				ftpClient.storeFile(
						file.getName() + System.currentTimeMillis(), stream);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				if (stream != null)
					try {
						stream.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
			filesSent.incrementAndGet();

		}
	}
	
	/*private static void exchangeFile(File file, FTPClient ftpClient) {

	//  System.out.println(file.getAbsolutePath());
			InputStream stream = TestFTP.class.getResourceAsStream(file
					.getName());

			try {
				ftpClient.storeFile(
						UUID.randomUUID().toString(), stream);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				if (stream != null)
					try {
						stream.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
			filesSent.incrementAndGet();

		
	}*/

	private static List<FTPClient> ftpConnection(int totalClients)
			throws SocketException, IOException {
		List<FTPClient> ftpClientList = new ArrayList<FTPClient>();
		for (int i = 0; i < totalClients; i++) {

			String server = "localhost";
			int port = 21;
			String user = "ftp_plain";
			String pass = "123";
			FTPClient ftpClient = new FTPClient();
			ftpClient.connect(server, port);
			int replyCode = ftpClient.getReplyCode();
			if (!FTPReply.isPositiveCompletion(replyCode)) {
				System.out.println("Operation failed. Server reply code: "
						+ replyCode);
				throw new RuntimeException(
						"Operation failed. Server reply code: " + replyCode);
			}
			boolean success = ftpClient.login(user, pass);
			if (!success) {
				System.out.println("Could not login to the server");
				throw new RuntimeException("connection to server failed.");
			} else {
				System.out.println("LOGGED IN SERVER");
				ftpClientList.add(ftpClient);
			}

		}

		return ftpClientList;
	}

}

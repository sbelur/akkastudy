/**
 * 
 */
package misc.collections;

import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

import com.cleo.lexicom.Schedule.Item;

/**
 * @author ragarwal
 *
 */
public class TestListForJava8Filtering extends TestListWithIteration{

  public static void main(String[] args) throws Exception {
    
    dumpStats();
    long startTime = System.currentTimeMillis();
    new TestListForJava8Filtering().performTest();
    long endTime = System.currentTimeMillis();
    System.out.println("Time Taken = " + (endTime-startTime));
    dumpStats();
  }

  public  void performTest() throws Exception {
    Vector<Item> vector = new Vector<Item>();
    populateVector(vector);
    for(int i=0;i<accessCycles;i++) {
      vector.stream().filter(item -> item.getItemTrigger() !=null).collect(Collectors.toList());
    }
  }
}

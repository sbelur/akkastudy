/**
 * 
 */
package misc.collections;

import java.util.List;
import java.util.Vector;

import com.cleo.lexicom.Schedule.Item;
import com.cleo.lexicom.external.ISchedule.TRIGGER_TYPE;
import com.cleo.lexicom.external.pojo.Schedule;

/**
 * @author ragarwal
 *
 */
public class TestListWithIteration {

  public static int itemCount  = 100000;
  public static int accessCycles = 20;
  public static void main(String[] args) throws Exception {
    long startTime = System.currentTimeMillis();
    new TestListWithIteration().performTest();
    dumpStats();
    long endTime = System.currentTimeMillis();
    System.out.println("time taken = " + (endTime-startTime));
  }

  public  void performTest() throws Exception {
    Vector<Item> vector = new Vector<Item>();
    populateVector(vector);
    for (int i=0;i<accessCycles;i++) {
      Vector vector2 = new Vector();
      for (Object object : vector) {
        vector2.add(object);  
      }
    }
  }

  public static void populateVector(List vector) throws Exception {
    com.cleo.lexicom.Schedule schedule = new com.cleo.lexicom.Schedule();
    for(int i=0;i<itemCount;i++) {
      Item newItem = schedule.newItem(new String[]{"test\\test1\\test2"});
      newItem.setItemTrigger(TRIGGER_TYPE.NewFileCreated);
      vector.add(newItem);
    }
  }



  public static void dumpStats() {
    /**
     * Class: TestMemory
     * @author: Viral Patel
     * @description: Prints JVM memory utilization statistics
     */


    int mb = 1024*1024;

    //Getting the runtime reference from system
    Runtime runtime = Runtime.getRuntime();

    //Print used memory
    System.out.println("Used Memory:"
        + (runtime.totalMemory() - runtime.freeMemory()) / mb);

  }
}

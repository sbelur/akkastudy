package research;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import java.util.concurrent.CyclicBarrier;

/**
 * Created by sbelur on 27/10/15.
 */
public class AKKA {
    private static CyclicBarrier taskBarrier = new CyclicBarrier(2, () -> System.out.println("One run over w/ task count " + Tester.totalTaskCount));
    private static akka.actor.ActorSystem system;

    static void initAKKA(boolean persist) {
        Config parseFile = null;
        if (persist) {
            parseFile = ConfigFactory.parseResources("singlevm.conf");
            ConfigFactory.load(parseFile);
            System.out.println(parseFile);
        } else {
            parseFile = ConfigFactory.parseResources("research/test.props");
            ConfigFactory.load(parseFile);
        }
        system = akka.actor.ActorSystem.create("ActorSampleSystem", parseFile);
    }

    public static akka.actor.ActorSystem getSystem() {
        return system;
    }


    public static CyclicBarrier getTaskBarrier() {
        return taskBarrier;
    }
}

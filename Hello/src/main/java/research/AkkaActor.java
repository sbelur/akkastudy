/**
 *
 */
package research;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

import akka.actor.ActorSelection;
import akka.dispatch.Futures;
import akka.dispatch.OnSuccess;

import com.cleo.labs.trigger.persist.akka.persistor.EventPersistor;
import com.cleo.labs.trigger.persist.akka.persistor.LexEventPersistor;
import com.cleo.labs.trigger.persist.akka.persistor.file.FilePersistor;
import com.cleo.labs.trigger.persist.akka.persistor.file.SessionCoordinator;
import com.cleo.util.logger.XMLLogElement;
import com.cleo.util.logger.external.LoggingException;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.pattern.Patterns;
import akka.routing.SmallestMailboxPool;
import research.Tester.Mode;
import research.serversimulator.ClientTest;
import research.task.BasicTask;
import research.task.PersistorTask;
import research.task.WorkerTask;
import research.task.ComplexCalculator;
import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.FiniteDuration;
import simplepersistance.TwoFilePersistor;
import static akka.dispatch.Futures.future;

/**
 * @author ragarwal
 */
public class AkkaActor {

	
    private static SessionCoordinator sessionCoordinator;
	
	 static void executeUsingAkkaActors(boolean persist)
			throws Exception {

		if (AKKA.getSystem() == null) {
			AKKA.initAKKA(persist);

			ActorRef target;
			ActorRef tracker;
			if (!persist) {
				target = AKKA.getSystem().actorOf(
						Props.create(WorkerCoordinator.class),
						"workerCoordinator");
				target.tell("start", ActorRef.noSender());
				sendUsingActorRefOrSel(persist, target);
			} else {
				// target =
				// AKKA.getSystem().actorOf(Props.create(SessionCoordinator.class),
				// "persistor");
				// System.out.println("target "+target);
				sessionCoordinator = new SessionCoordinator();
				tracker = AKKA.getSystem().actorOf(
						Props.create(WorkTracker.class).withDispatcher(
								"akka.actor.pinnedDispatcher"), "tracker");
				tracker.tell("start", ActorRef.noSender());
				sendUsingActorRefOrSel(persist, sessionCoordinator);
			}

		} else {
			ActorSelection target;
			if (!persist) {
				target = AKKA.getSystem().actorSelection(
						"/user/workerCoordinator");
				target.tell("start", ActorRef.noSender());
				sendUsingActorRefOrSel(persist, target);
			} else {

				// target = AKKA.getSystem().actorSelection("/user/persistor");
				// System.out.println(target);
				// target.tell("reset", ActorRef.noSender());

				System.out.println("next run");
				ActorSelection sel = AKKA.getSystem().actorSelection(
						"/user/tracker");
				sel.tell("start", ActorRef.noSender());
				sendUsingActorRefOrSel(persist, sessionCoordinator);

			}

		}

		AKKA.getTaskBarrier().await();

	}

	private static void sendUsingActorRefOrSel(boolean persist, Object target) throws Exception {
		System.out.println("Sending " + Tester.totalTaskCount + " messages to akka");
		for (int i = 0; i < Tester.totalTaskCount; i++) {

			if (persist) {
				XMLLogElement xmllogElement = Events.generateEvent();
				SampleMessage msg = new SampleMessage("sample detail message.", xmllogElement);
				msg.setStart(System.nanoTime());
				msg.setStartInMs(System.currentTimeMillis());
				if(target instanceof ActorRef) {
                    ((ActorRef) target).tell(msg, ActorRef.noSender());
                }
                else if(target instanceof SessionCoordinator){
                    ((SessionCoordinator)target).onReceive(msg);
                }
                else {
                    ((ActorSelection) target).tell(msg, ActorRef.noSender());
                }

            }  else {
				SampleMessage msg = new SampleMessage("sample detail message.");
				msg.setStart(System.nanoTime());
				if (target instanceof ActorRef) {
					((ActorRef) target).tell(msg, ActorRef.noSender());
				} else {

					((ActorSelection) target).tell(msg, ActorRef.noSender());
				}
			}
		}
	}

	  static long getExecutionTimesForAkka(int count) throws Exception {
	        ActorSystem system = AKKA.getSystem();
	        ActorSelection tracker = system.actorSelection("/user/tracker");
	        Future<Object> future = Patterns.ask(tracker, "alltimes", 5000L);
	        List<Long> times = (List<Long>) Await.result(future, new FiniteDuration(5000, TimeUnit.SECONDS));
	        long sum = 0;
	        System.out.println("times "+times);
	        for(int ctr=0;ctr<times.size();ctr++){
	            sum = sum + times.get(ctr);
	        }
	        System.out.println(sum + "," + count);
	        tracker.tell("resetTimes", ActorRef.noSender());
	        return (sum / count);
	    }
	 
	static void executeUsingAkkaFutures() throws Exception {
        Config parseFile = ConfigFactory.parseResources("research/test.props");
        ConfigFactory.load(parseFile);
        final ActorSystem system = ActorSystem.create("ActorSampleSystem", parseFile);
        final AtomicInteger count = new AtomicInteger(0);
        List<Future<Void>> list = new ArrayList<Future<Void>>();
        final long startTime = System.currentTimeMillis();
        for (int i = 0; i < Tester.totalTaskCount; i++) {
            Future<Void> aF = future(new Callable<Void>() {

                public Void call() throws Exception {
                    int i;
                    for (i = 0; i < 1000000; i++) {
                        for (int j = 0; j < 10000; j++) {
                            float comlexCal = 676764 ^ 676 ^ 676 * 898 / 67 * 4 ^ 7 ^ 7 * 89;
                            comlexCal = 676764 ^ 676 ^ 676 * 898 / 67 * 4 ^ 7 ^ 7 * 89;
                            for (int k = 0; k < 1000; k++) {
                                comlexCal = 676764 ^ 676 ^ 676 * 898 / 67 * 4 ^ 7 ^ 7 * 89;
                                comlexCal = 676764 ^ 676 ^ 676 * 898 / 67 * 4 ^ 7 ^ 7 * 89;
                            }
                        }
                    }
                    count.incrementAndGet();
                    return null;
                }
            }, system.dispatcher());
            list.add(aF);
        }

        Futures.sequence(list, system.dispatcher()).onSuccess(new OnSuccess() {
            @Override
            public void onSuccess(Object result) throws Throwable {
                System.out.println("total tasks in future " + count.get());
                System.out.println("total time " + (System.currentTimeMillis() - startTime) + "ms");
                system.shutdown();
            }
        }, system.dispatcher());

        system.awaitTermination(new FiniteDuration(20, TimeUnit.SECONDS));
    }

  static  HashMap<String, String> performTestAkka() throws Exception{
	  System.out.println("***collecting stats for Akka Actors***.");
		String executionMode = Tester.Mode.actors.toString();
		System.setProperty(Tester.EXECUTION_MODE, executionMode);
		Tester.startMonitoring();
		Tester.collectStatsForExecutionMode(executionMode, Tester.initialCount);
		HashMap<String, String> metricMap = Tester.stopMonitoring();
		return metricMap;
  }
}





/**
 * 
 */
package research;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.DeadLetter;
import akka.actor.Props;
import akka.actor.UntypedActor;

/**
 * @author ragarwal
 *
 */
public class DeadLetterSample {
	
	public static class ProcessActor extends UntypedActor {

		@Override
		public void onReceive(Object paramObject) throws Exception {
			System.out.println("recived the message "+ paramObject +". adding something to dead letter queue");
			getSender().tell("Dead Message", getSelf());
		}
	}
	
	public static class DeadLetterActor extends UntypedActor {

		@Override
		public void onReceive(Object paramObject) throws Exception {
			System.out.println("recieved the dead message " + paramObject);
		}
		
	}
	
	public static void main(String[] args) {
		ActorSystem akkaSystem = ActorSystem.create("DeadMessages");
		ActorRef processor = akkaSystem.actorOf(Props.create(ProcessActor.class), "Processor");
		processor.tell("Message for Processor", ActorRef.noSender());
		
		ActorRef deadLetterActor = akkaSystem.actorOf(Props.create(DeadLetterActor.class),"DeadLetters");
		akkaSystem.eventStream().subscribe(deadLetterActor, DeadLetter.class);
		akkaSystem.shutdown();
	}

}

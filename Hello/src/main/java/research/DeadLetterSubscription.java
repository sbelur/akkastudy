/**
 * 
 */
package research;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.DeadLetter;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.dispatch.sysmsg.Suspend;

/**
 * @author ragarwal
 *
 */
public class DeadLetterSubscription {
	
	public static class MessageProcessor extends UntypedActor {

		/* (non-Javadoc)
		 * @see akka.actor.UntypedActor#onReceive(java.lang.Object)
		 */
		@Override
		public void onReceive(Object paramObject) throws Exception {
			System.out.println("recived message of type " + paramObject.getClass());
			getSender().tell(new SubscribedMessage("test"), getSelf());
		}
	}
	
	public static class MessageSubscriber extends UntypedActor {

		/* (non-Javadoc)
		 * @see akka.actor.UntypedActor#onReceive(java.lang.Object)
		 */
		@Override
		public void onReceive(Object paramObject) throws Exception {
			System.out.println("got the message delivered" + paramObject.getClass());
		}
		
	}
	
	public static void main(String[] args) {
		ActorSystem system = ActorSystem.create("MessageSubscriptionSampleSystem");
		ActorRef messageProcessorActor = system.actorOf(Props.create(MessageProcessor.class), 
				MessageProcessor.class.getCanonicalName());
		
		ActorRef subscriberActor = system.actorOf(Props.create(MessageSubscriber.class), MessageSubscriber.class.getCanonicalName());
		system.eventStream().subscribe(subscriberActor, DeadLetter.class);
		messageProcessorActor.tell("Create a subscription message.", ActorRef.noSender());
		system.shutdown();
	}

}

class SubscribedMessage {
	
	private String messageDetails;

	public SubscribedMessage(String messageDetails) {
		super();
		this.messageDetails = messageDetails;
	}

	/**
	 * @return the messageDetails
	 */
	public String getMessageDetails() {
		return messageDetails;
	}
}

package research.H2Database;

import java.sql.*;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.h2.jdbcx.JdbcConnectionPool;

import com.cleo.util.logger.XMLLogElement;

import research.Events;
import research.Tester;
import research.Tester.Mode;
import research.task.WorkerTask;
import research.triggers.*;
import simplepersistance.TwoFilePersistor;
import simplepersistance.TwoFilePersistor.PERSISTANCE_TYPE;

public class H2Database {

	private final static String driverName = "org.h2.Driver";
	static JdbcConnectionPool cp = null;
	private Connection conn = null;
	private static String dburl = "jdbc:h2:.";
	static FileCreatedTrigger trigger = TriggerUtils.createSampleTrigger();
	static {
	    
		Statement stmt = null;
		Connection connection = null;
		try {
			Class.forName(driverName);
			 cp = JdbcConnectionPool.create(dburl, "sa",
					"sa");
			connection = cp.getConnection();
			
			  stmt = connection.createStatement(); String createStmt =
			  "create table if not exists " +
			  "FILE_TRIGGER(uuid varchar(100), triggerdata varchar(2000), status int)"
			  ; stmt.executeUpdate(createStmt); stmt.close();
			  stmt.close();
			  connection.close();
		} catch (Exception e) {
			try {
				throw e;
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
					connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}



	public H2Database() {
		// TODO Auto-generated constructor stub
		try {
			this.conn = cp.getConnection();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	public  int getNumRows() throws Exception {
		Statement statement = null;

		try {
			String countSql = "select count(*) FILE_TRIGGER";
			statement = conn.createStatement();
			ResultSet rs = statement .executeQuery(countSql);
			rs.next();
			return rs.getInt(1);

		} catch (Exception e) {
			throw e;
		} finally {
			if (statement != null)
				statement.close();
		}
	}

	public  void removeAllRows() throws Exception {
		PreparedStatement statement = null;

		try {
			String deleteSql = "delete from FILE_TRIGGER";
			statement = conn.prepareStatement(deleteSql);
			statement.execute();
		} catch (Exception e) {
			throw e;
		} finally {
			if (statement != null)
				statement.close();
		}
	}

	public void close() throws Exception {
		if (conn != null) {
			conn.close();
		}
	}

	public void addTriggerToStore(String uuid) throws Exception {
		PreparedStatement statement = null;

		try {
			String insertSql = "insert into FILE_TRIGGER values (?,?,?)";
			statement = conn.prepareStatement(insertSql);
			statement.setString(1, uuid);
			statement.setString(2, trigger.toString());
			statement.setInt(3, 0);
			statement.execute();
		} catch (Exception e) {
			throw e;
		} finally {
			statement.close();
		}
	}

	public void triggerDone(String uuid) throws Exception {
		PreparedStatement statement = null;
		try {
			String updateSql = "update FILE_TRIGGER set status = ? where uuid like ?";
			statement = conn.prepareStatement(updateSql);
			statement.setInt(1, 1);
			statement.setString(2, uuid);
			statement.execute();
		} catch (Exception e) {
			throw e;
		} finally {
			if (statement != null) {
				statement.close();
			}
		}
	}

	public void printRows() throws Exception {
		String sql = "SELECT * from FILE_TRIGGER";
		Statement stmt = null;
		ResultSet rs = null;
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				String uuid = rs.getString("uuid");
				int status = rs.getInt("status");
				System.out.println("uuid = " + uuid + " status= " + status);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			if (rs != null) {
				rs.close();
			}
			if (stmt != null) {
				stmt.close();
			}
		}

	}

	public void persistTrigger(String uuid)
			throws Exception {

		addTriggerToStore(uuid);
		triggerDone(uuid);
	}

	public static void main(String args[]) throws Exception {

		// Pass the database name, user and password
		// If the database does not exist it is created
		H2Database conToH2 = new H2Database();

		conToH2.addTriggerToStore("uuid1");
		conToH2.addTriggerToStore("uuid2");
		conToH2.addTriggerToStore("uuid3");
		conToH2.addTriggerToStore("uuid4");

		conToH2.triggerDone("uuid1");
		conToH2.triggerDone("uuid2");
		System.out.println(conToH2.getNumRows());

		conToH2.printRows();
		conToH2.removeAllRows();
		conToH2.close();
	}

	public static HashMap<String, String> performTestSingularPersist()
			throws Exception {
		System.out
				.println("***collecting stats for jdk threads with H2Database Persistance***.");
		System.setProperty(Tester.PROP_NAME_PERSIST, "true");
		System.setProperty(Tester.PROP_NAME_PERSIST_STRATEGY,
				PERSISTANCE_TYPE.SINGLUAR.toString());
		String executionMode = Mode.h2Database.toString();
		System.setProperty(Tester.EXECUTION_MODE, executionMode);
		Tester.startMonitoring();
		Tester.collectStatsForExecutionMode(executionMode,
				Tester.initialCount);
		HashMap<String, String> metricMap = Tester.stopMonitoring();
		return metricMap;

	}
}

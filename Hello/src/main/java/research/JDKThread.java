package research;

import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import research.Tester.Mode;
import research.task.BasicTask;
import research.task.ComplexCalculator;
import research.task.PersistorTask;
import research.task.WorkerTask;
import research.triggers.TriggerUtils;
import simplepersistance.IEventPersistor;
import simplepersistance.PersistorFactory;
import simplepersistance.TwoFilePersistor;

import com.cleo.util.logger.XMLLogElement;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

public class JDKThread {
	
	static LinkedBlockingQueue<Runnable> workQueue = new LinkedBlockingQueue<Runnable>();
	static int total = Runtime.getRuntime().availableProcessors() * 2;
	
	static {
		String totalThread = System.getProperty(Tester.PROP_NAME_JDK_THREADS_COUNT);
		if(totalThread!=null) {
			total = Integer.parseInt(totalThread);
			System.out.println("Prop set Total threads used for execution are " + total);
		} else {
			System.out.println("Prop not set Total threads used for execution are " + total);
		}
	}
	
	static ExecutorService pool = new ThreadPoolExecutor(total, total, 600L, TimeUnit.SECONDS, workQueue);

	public static ExecutorService getWorkerPool() {
		return pool;
	}
	
	 static void configureSameNumberJDKThreadsAsForAkka() {
		Config configFile = ConfigFactory.parseResources("singlevm.conf");
		Config loadedConfig = ConfigFactory.load(configFile);
		Object parallelFactor = loadedConfig.getAnyRef("akka.actor.default-dispatcher.fork-join-executor.parallelism-factor");

		// setting the same threads for jdk threads.
		int parallel = Integer.parseInt(parallelFactor.toString());
		System.setProperty(Tester.PROP_NAME_JDK_THREADS_COUNT, (Runtime.getRuntime().availableProcessors()*parallel)+"");
	}
	
	 static void executeUsingJDKExecutors() throws Exception {
		ComplexCalculator.cleanSet();
		boolean persistProperty = Tester.getPersistProperty();

		String persistStrategy = System.getProperty(Tester.PROP_NAME_PERSIST_STRATEGY);
		IEventPersistor persistor = PersistorFactory.getInstance(TwoFilePersistor.PERSISTANCE_TYPE.valueOf(persistStrategy));
		WorkerTask task = new BasicTask("testing", null);
		for (int i = 0; i <Tester.totalTaskCount; i++) {
			if(persistProperty) {
				task = new PersistorTask("test", TriggerUtils.createSampleTrigger(),persistor);
				pool.execute(task);
			} else {
				pool.execute(task);
			}
		}
		while(ComplexCalculator.ProcessedTasks.get()<Tester.totalTaskCount-1) {
			Thread.sleep(10);
		}
		System.out.println("Total tasks processed by Thread Pool " + ComplexCalculator.ProcessedTasks);
	}
	 
	 static HashMap<String,String> performTestJdkThreads() throws Exception{
		 System.out.println("***collecting stats for jdk threads***.");
		 String executionMode = Mode.jdk_threads.toString();
			System.setProperty(Tester.EXECUTION_MODE, executionMode);
	        Tester.startMonitoring();
			Tester.collectStatsForExecutionMode(executionMode, Tester.initialCount);
			HashMap<String, String> metricMap = Tester.stopMonitoring();
			return metricMap;
			
	  }
}

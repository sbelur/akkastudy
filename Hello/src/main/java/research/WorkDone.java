package research;

/**
 * Created by sbelur on 18/10/15.
 */
public class WorkDone {


    private  Long start = -1L;
    private Long end = -1L;

    public WorkDone(Long start, Long end){
        this.start = start;
        this.end = end;
    }

    public Long getStart() {
        return start == null ? -1L : start;
    }

    public Long getEnd() {
        return end == null ? -1L: end;
    }
}

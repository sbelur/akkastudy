package research;

import akka.actor.*;

import static akka.actor.OneForOneStrategy.*;

import com.cleo.util.logger.XMLLogElement;

import akka.japi.Function;
import akka.routing.*;
import scala.concurrent.duration.Duration;

/**
 * Created by sbelur on 18/10/15.
 */
public class WorkerCoordinator extends UntypedActor{


    private static final String WORKER_IDENTIFIER = "Identifier";

    private ActorRef workerRouter;
    private ActorRef workerTracker;

    private int workDone = 0;

    private long startTime = 0;

    @Override
    public void preStart() throws Exception {
        super.preStart();
        workerTracker = context().actorOf(Props.create(WorkTracker.class));
        final int total = Runtime.getRuntime().availableProcessors()+1;
        workerRouter = getContext().system().actorOf(new SmallestMailboxPool(total).props(Props.create(WorkerActor.class, WORKER_IDENTIFIER)),
                "workerRouter");
        /* workerRouter = getContext().system().actorOf(new ConsistentHashingPool(total).withHashMapper(new ConsistentHashingRouter.ConsistentHashMapper() {

            @Override
            public Object hashKey(Object message) {
                if (message instanceof SampleMessage) {
                    int hashingId = ((SampleMessage) message).getSid();
                    return hashingId;
                }
                return null;
            }
        }).props(Props.create(WorkerActor.class, WORKER_IDENTIFIER)),
                "workerRouter");*/
    }

    private static SupervisorStrategy strategy =
            new OneForOneStrategy(-1, Duration.Inf(),
                    new Function<Throwable, SupervisorStrategy.Directive>() {
                        public SupervisorStrategy.Directive apply(Throwable t) {
                            if (t instanceof ActorKilledException) {
                                return escalate();
                            } else if (t instanceof ActorInitializationException) {
                                return escalate();
                            } else {
                                return resume();
                            }
                        }
                    });

    @Override
    public SupervisorStrategy supervisorStrategy() {
        return strategy;
    }

    @Override
    public void onReceive(Object message) throws Exception {
    	
        if("start".equals(message)){
            workerTracker.tell("start",getSelf());
        }
        else if(message instanceof SampleMessage){
 
        	SampleMessage sample = (SampleMessage)message;
        	
        	//Swaroop, the XMLLogElement event is already being passed
        	//You can persist this element
        	XMLLogElement element = sample.getElement();
            workerRouter.tell(message,workerTracker);
        }
        else {
            unhandled(message);
        }
       /* else if(message instanceof WorkDone){
            workDone++;
            *//*if(workDone % 1000 == 0){
                System.out.println("workDone "+workDone);
            }*//*
            if(workDone == ActorSample.totalTaskCount){
                getContext().stop(getSelf());
            }
        }*/
    }

  /*  @Override
    public void postStop() throws Exception {
        System.out.println("Total work done "+workDone);
        System.out.println("Total time taken " + (System.currentTimeMillis() - startTime) + " ms");
        getContext().system().shutdown();
    }*/
}

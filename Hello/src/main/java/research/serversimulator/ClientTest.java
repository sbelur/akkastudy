/**
 * 
 */
package research.serversimulator;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.net.URL;
import java.net.UnknownHostException;

/**
 * @author sbanerjee
 *
 */
public class ClientTest {

	/**
	 * @param args
	 * @throws IOException 
	 * @throws UnknownHostException 
	 */
	public static void main(String[] args) throws UnknownHostException, IOException {
		// TODO Auto-generated method stub
		URL resource = ClientTest.class.getResource(".");
		//System.out.println(resource);
		String classPath = resource.getFile();
		classPath = classPath.replace("research/serversimulator/", "");
		System.out.println(classPath);
		//System.out.println(classPath);
	    Process process = Runtime.getRuntime().exec("java -cp " + classPath+ " research.serversimulator.ServerTest");
	    for(int i=0;i<2000;i++) {
	    	System.out.println("" + i);
	    contactServer();
	    }
	    process.destroyForcibly();
	        

	}
	
	public static void contactServer() throws UnknownHostException, IOException {
		Socket socket = new Socket("localhost", 9052);
		BufferedReader input = new BufferedReader(new InputStreamReader(
				socket.getInputStream()));
		String answer = input.readLine();
		// System.out.println(answer);
		socket.close();
	}

}

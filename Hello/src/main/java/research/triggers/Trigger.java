package research.triggers;

import java.util.UUID;



public abstract class Trigger {
	public enum Status {SUCCESS, FAILURE};
	public enum Protocol {FTP, SSHFTP, LCOPY};
	
	private String uuid = UUID.randomUUID().toString();

	/**
	 * @return the uuid
	 */
	public String getUuid() {
		return uuid;
	}
	
}

package simplepersistance;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import com.cleo.lexicom.XMLBeaner;
import com.cleo.lexicom.beans.LexBeanLogEvent;
import com.cleo.lexicom.beans.LexFile;
import com.cleo.lexicom.external.IActionController;
import com.cleo.lexicom.external.ILexiCom;
import com.cleo.lexicom.external.IMailboxController;
import com.cleo.lexicom.external.LexiComFactory;
import com.cleo.util.logger.XMLLogElement;

/**
 * Created by sbelur on 20/09/15.
 */
public class ExecutionUtil {

	private static HashMap<String, HashMap<String, String>> sessionDetails = new HashMap<String, HashMap<String, String>>(); 
	public static ExecutorService workerPool = Executors.newFixedThreadPool(200);
	public static ExecutionType eventHandlingType = ExecutionType.SYNC;
	public static Persist persistEvents = Persist.TRUE; 
	public static ILexiCom lexicom = null;

	public static enum ExecutionType {
		SYNC, ASYNC;  
	}

	public static enum Persist {
		TRUE,FALSE;
	}

	static {
		init(); 
		initLexicom();
	}

	private static void initLexicom() {
		try {
			lexicom = LexiComFactory.getCurrentInstance();
		} catch (Exception e) {
			System.out.println("\nignoring lexicom init exception\n");
		}
	}

	private static void init() {
		try {
			String confFile = "events.conf";
			if(!new LexFile(confFile).exists()) {
				System.out.println("No action configured for sucessful file transfer."); 
				return;
			}
			
			FileInputStream stream = getConfStream(confFile);
			Properties props = new Properties();
			props.load(stream);
			String property = props.getProperty("ExecutionType");
			if(property!=null) {
				eventHandlingType = ExecutionType.valueOf(property);
			}
			property = props.getProperty("Persist");
			if(property!=null) {
				persistEvents = Persist.valueOf(property);
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}


	public static Element getElement(XMLLogElement event) throws Exception{

		XMLBeaner xmlbeaner = new XMLBeaner(null);
		Document document = xmlbeaner.newDocument();

		if (event == null)
			return null;

		Element elem = document.createElement(event.name());
		for (int i = 0; i < event.getLength(); i++) {
			if (event.getLocalName(i).equals("%"))
				elem.setAttribute("percentage", event.getValue(i));
			else
				elem.setAttribute(event.getLocalName(i), event.getValue(i));
		}
		if (event.content() != null && event.content().length() > 0)
			elem.appendChild(document.createTextNode(event.content()));

		return elem;
	}


	public static void handleEvent(LexBeanLogEvent event) throws Exception {
		XMLLogElement xmlEvent = event.getEvent();
		Element element = getElement(xmlEvent);
		if(!isEventOfInterset(element)) return;
		UUID randomUUID = UUID.randomUUID();
		if(persistEvents.equals(Persist.TRUE)) {
			PersistorFactory.getInstance(null).persistMessage(element,randomUUID.toString());
		}
		if(eventHandlingType.equals(ExecutionType.ASYNC)) {
			workerPool.submit(new EventExecutorTask(element));
		} else {
			processEvent(element);
		}
		if(persistEvents.equals(Persist.TRUE)) {
			PersistorFactory.getInstance(null).deleteMessage(randomUUID.toString());
		}
	}

	public static boolean isEventOfInterset(Element element) {
		NamedNodeMap attributes = element.getAttributes();
		Node namedItem = attributes.getNamedItem("Socket");
		return namedItem!=null;
	}

	public static void processEvent(Element element) throws Exception,
	FileNotFoundException, IOException {


		NamedNodeMap attributes = element.getAttributes();
		Node namedItem = attributes.getNamedItem("Socket");
		if(!isEventOfInterset(element)) return;
		for(int i=0;i<attributes.getLength();i++) {
			Node item = attributes.item(i);
			HashMap<String, String> hashMap = sessionDetails.get(namedItem.getNodeValue().toString());
			if(hashMap==null) {
				hashMap = new HashMap<String, String>();
				sessionDetails.put(namedItem.getNodeValue().toString(), new HashMap<String, String>());
			} else {
				if(item.getNodeName().equalsIgnoreCase("secondsConnected")) {
					handleLogout(hashMap);
				}
				if(item.getNodeName().equalsIgnoreCase("destination")) {
					String newDestination = item.getNodeValue();
					String destValue = hashMap.get("destination");
					if(destValue!=null) {
						hashMap.put("destination", destValue+"," + newDestination);
					} else {
						hashMap.put("destination", newDestination);
					}
				}
			}
			if(!item.getNodeName().equalsIgnoreCase("destination")) {
				hashMap.put(item.getNodeName(), item.getNodeValue());
			}
		}
		runEvent(lexicom, element);
	}


	private static void runEvent(ILexiCom lexicom, Element element) throws FileNotFoundException, IOException,
	Exception {
			if(isFileEvent(element)) {
				String confValues = getConfData("action.conf");
				executeAction(lexicom, element, confValues);
		}
	}


	private static void executeAction(ILexiCom lexicom, Element element,
			String confValues) throws Exception {
		String[] actionDetails = confValues.split("\\\\");

		String mailBox = actionDetails[1];
		String destination = element.getAttribute("destination");
		if(destination==null) {
			System.out.println("destination is mandatory, not proceeding further.");
			return;
		}
		if(mailBox == null || !destination.contains(mailBox)) {
			System.out.println("Nothing matching found for the mail box and destination. hence not going ahead."
					+ " Where host details are :: " + confValues + " destination = " + destination);
			return;
		}
		runAction(lexicom, actionDetails);
	}


	private static void runAction(ILexiCom lexicom, String[] actionDetails)
			throws Exception {
		IMailboxController mailboxController = lexicom.getMailboxController(new String[]{actionDetails[0],actionDetails[1]});
		IActionController actionController = mailboxController.getActionController(actionDetails[2]);
		actionController.execute(actionController.getBaseCommands()[0]);
		actionController.end();
	}


	private static String getConfData(String confFile) throws FileNotFoundException,
	IOException {
		LexFile lexFile = new LexFile(confFile);
		if(!lexFile.exists()) {
			System.out.println("No action configured for sucessful file transfer."); 
		}
		FileInputStream stream = new FileInputStream(lexFile);
		byte[] data = new byte[new Long(lexFile.length()).intValue()];
		stream.read(data);
		stream.close();
		String confValues = new String(data);
		return confValues;
	}

	private static FileInputStream getConfStream(String confFile) throws FileNotFoundException,
	IOException {
		LexFile lexFile = new LexFile(confFile);
		if(!lexFile.exists()) {
			System.out.println("No action configured for sucessful file transfer."); 
		}
		return new FileInputStream(lexFile);
	}


	private static boolean isFileEvent(XMLLogElement xmlEvent) {
		return xmlEvent.getValue("direction")!=null && xmlEvent.getValue("direction").contains("Host->Local")  
				&& xmlEvent.getValue("text")!=null && xmlEvent.getValue("text").contains("Success");
	}
	
	private static boolean isFileEvent(Element element) {
		return element.getAttribute("direction")!=null && element.getAttribute("direction").contains("Host->Local")  
				&& element.getAttribute("text")!=null && element.getAttribute("text").contains("Success");
	}

	/**
	 * @param event
	 * @param hashMap
	 */
	private static void handleLogout(HashMap<String, String> hashMap) throws Exception  {
		String confValues = getConfData("logout.conf");
		String[] actionDetails = confValues.split("\\\\");
		runAction(lexicom, actionDetails);
	}


	/**
	 * @param commands
	 * @param actionAliasPath
	 * @param lexicom
	 */
	public static void execute(String[] commands, String[] actionAliasPath,
			ILexiCom lexicom) {
		// TODO Auto-generated method stub

	}

}

/**
 * 
 */
package simplepersistance;


/**
 * @author ragarwal
 *
 */
public interface IEventPersistor {
	
	public void persistMessage(Object event, String uuid) throws Exception;
	
	public void deleteMessage(String uuid) throws Exception;


}

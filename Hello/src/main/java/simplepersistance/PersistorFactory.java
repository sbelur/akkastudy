/**
 * 
 */
package simplepersistance;

import java.util.HashMap;

import simplepersistance.TwoFilePersistor.PERSISTANCE_TYPE;

/**
 * @author ragarwal
 *
 */
public class PersistorFactory {
	
	public static HashMap<PERSISTANCE_TYPE, IEventPersistor> instanceMap = new HashMap<TwoFilePersistor.PERSISTANCE_TYPE, IEventPersistor>();
	
	static  {
		TwoFilePersistor persistor = new TwoFilePersistor();
		instanceMap.put(PERSISTANCE_TYPE.SINGLUAR, persistor);
//		schedulePoolConcurrent.scheduleAtFixedRate(new CleanupTask(persistor), 5, 5, TimeUnit.MINUTES);
		ConcurrentEventPersistor conPersistor = new ConcurrentEventPersistor();
		instanceMap.put(PERSISTANCE_TYPE.CONCURRENT, conPersistor);
//		schedulePoolConcurrent.scheduleAtFixedRate(new CleanupTask(conPersistor), 5, 5, TimeUnit.MINUTES);
		instanceMap.put(PERSISTANCE_TYPE.SINGLUAR_ONE_FILE, SingleFilePersistor.getInstance());
		
		JsonFilePersistor jsonPersistor = new JsonFilePersistor();
		instanceMap.put(PERSISTANCE_TYPE.JSON, jsonPersistor);
		
		
	}

	public static synchronized IEventPersistor getInstance(PERSISTANCE_TYPE type) {
		if(type == null || type.equals(PERSISTANCE_TYPE.SINGLUAR)) {
			TwoFilePersistor eventPersistor = (TwoFilePersistor)instanceMap.get(PERSISTANCE_TYPE.SINGLUAR);
			return eventPersistor;
		}
		
		if(type.equals(PERSISTANCE_TYPE.SINGLUAR_ONE_FILE)) {
			return instanceMap.get(PERSISTANCE_TYPE.SINGLUAR_ONE_FILE);
		}
		
		if(type.equals(PERSISTANCE_TYPE.CONCURRENT)) {
			return instanceMap.get(PERSISTANCE_TYPE.CONCURRENT);
		}
		
		if(type.equals(PERSISTANCE_TYPE.JSON)) {
			return instanceMap.get(PERSISTANCE_TYPE.JSON);
		}

		throw new RuntimeException("Not able to find persistor for input type " + type);
	}
}

package testClients;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.couchbase.client.core.annotations.InterfaceAudience.Private;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import junit.framework.TestCase;

/**
 * 
 * @author manjunathm
 *
 */
public class SFTPFileUpload {


	final static String HARMONY_HOME = "D:\\cleo\\5.3\\Harmony";

	  /*Runtime runtime = Runtime.getRuntime();
		Process process = runtime.exec(HARMONY_HOME+"\\Harmonyc.exe");
		sftp.send();
		FileReader fs = new FileReader(HARMONY_HOME+"\\logs\\Harmony.dbg");
		File file = new File("C:\\Users\\manjunathm\\Desktop\\test.txt");
		sftp.findWord("12:37:45 : restartAllowed: false", file);*/
	

	public boolean findWord(String word, File file) throws Exception {

		Scanner scanner = new Scanner(file);
		while (scanner.hasNextLine()) {
			String nextToken = scanner.next();
			if (nextToken.equalsIgnoreCase(word)){
				System.out.println("found");
				return true;
			}
		}
		return false;
	}

	public void sshFileUpload() throws Exception {
		String SFTPHOST = "localhost";
		int SFTPPORT = 5022;
		String SFTPUSER = "sshftp_plain";
		String SFTPPASS = "cleo";

		Session session = null;
		Channel channel = null;
		ChannelSftp channelSftp = null;
		System.out.println("preparing the host information for sftp.");
		try {
			JSch jsch = new JSch();
			session = jsch.getSession(SFTPUSER, SFTPHOST, SFTPPORT);
			session.setPassword(SFTPPASS);
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			session.connect();
			System.out.println("Host connected.");
			channel = session.openChannel("sftp");
			channel.connect();
			System.out.println("sftp channel opened and connected.");
			channelSftp = (ChannelSftp) channel;
			File f = createOneDataFile();
			channelSftp.put(new FileInputStream(f), f.getName());
			System.out.println("File transfered successfully to host.");
		} catch (Exception ex) {
			System.out.println("Exception found while tranfer the response.");
		}
		finally{
			channelSftp.exit();
			System.out.println("sftp Channel exited.");
			channel.disconnect();
			System.out.println("Channel disconnected.");
			session.disconnect();
			System.out.println("Host Session disconnected.");
		}
	}

	public static File createOneDataFile() throws Exception{
		URL resource = SFTPFileUpload.class.getResource(".");
		File file = new File(resource.getFile()+"test.data");
		FileOutputStream stream = new FileOutputStream(file);
		stream.write("simple test data".getBytes());
		stream.close();
		return file;
	}
}
